

import turtle
import random

wn = turtle.Screen()
wn.title("Ludo King For Sheetal--------")
wn.bgcolor("#994444")
wn.setup(width = 800 , height= 600)
wn.tracer(0)

def throw_dice():
    return (random.randint(1,6))

def make_square(turtle , s_a , s_b , x , y):
    wn0 = turtle.Turtle()
    wn0.shape("square")
    wn0.fillcolor("white")
    wn0.shapesize(stretch_len=s_a , stretch_wid=s_b)
    wn0.penup()
    wn0.goto(x , y)

def make_turtle(color , x , y):
    bob = turtle.Turtle()
    bob.shape("turtle")
    bob.fillcolor(str(color))
    bob.shapesize(stretch_len=1.5 , stretch_wid=1.5)
    bob.penup()
    bob.goto(x , y)

def make_triangle(x , y , color , angle):
    bob = turtle.Turtle()
    bob.shape("triangle")
    bob.fillcolor(str(color))
    bob.shapesize(stretch_wid=6 , stretch_len=3)
    bob.penup()
    bob.goto(x,y)
    bob._rotate(angle)



def make_row(x,y):
    make_square(turtle, 2, 2, x * 2, y)
    make_square(turtle, 2, 2, x * 3, y)
    make_square(turtle, 2, 2, x * 4, y)
    make_square(turtle, 2, 2, x * 5, y)
    make_square(turtle, 2, 2, x * 6, y)


def make_col(x, y):
    make_square(turtle, 2, 2, x , y * 2)
    make_square(turtle, 2, 2, x , y * 3)
    make_square(turtle, 2, 2, x , y * 4)
    make_square(turtle, 2, 2, x , y * 5)
    make_square(turtle, 2, 2, x , y * 6)





#initial
make_square(turtle , 6 , 6 , 0 , 0)
make_triangle(-42 , 0 , "red" , 0)
make_triangle(0 , -42 , "blue" , 90)
make_triangle(0 , 42 , "green" , 270)
make_triangle(42 , 0 , "yellow" , 180)


make_row(40, 40)
make_row(40 , 0)
make_row(40 , -40)
make_row(-40, 40)
make_row(-40 , 0)
make_row(-40 , -40)

make_col(40 , 40)
make_col(0 , 40)
make_col(-40 , 40)
make_col(40 , -40)
make_col(0 , -40)
make_col(-40 , -40)

def team(x , y , color):
    make_square(turtle , 6 , 6 , x , y)
    make_turtle(color, x+30 , y+30)
    make_turtle(color, x-30 , y+30)
    make_turtle(color, x-30 , y-30)
    make_turtle(color, x+30 , y-30)





#team yellow
make_square(turtle , 6 , 6 , 180 , -180)
turtle_y_1 = make_turtle("yellow", 210 , -210)
turtle_y_2 = make_turtle("yellow", 150 , -210)
turtle_y_3 = make_turtle("yellow", 210 , -150)
turtle_y_4 = make_turtle("yellow", 150 , -150)

#team red
make_square(turtle , 6 , 6 , -180 , 180)
turtle_r_1 = make_turtle("red", -210 , 210)
turtle_r_2 = make_turtle("red", -150 , 210)
turtle_r_3 = make_turtle("red", -210 , 150)
turtle_r_4 = make_turtle("red", -150 , 150)

#team blug
make_square(turtle, 6  , 6 , -180 , -180)
turtle_b_1  =  make_turtle("blue", -210 , -210)
turtle_b_2  =  make_turtle("blue", -150 , -210)
turtle_b_3  =  make_turtle("blue", -210 , -150)
turtle_b_4  =  make_turtle("blue", -150 , -150)

#team_green
make_square(turtle , 6 , 6 , 180 , 180)
turtle_g_1 = make_turtle("green", 210 , 210)
turtle_g_2 = make_turtle("green", 150 , 210)
turtle_g_3 = make_turtle("green", 210 , 150)
turtle_g_4 = make_turtle("green", 150 , 150)




while True:
    wn.update()
 
